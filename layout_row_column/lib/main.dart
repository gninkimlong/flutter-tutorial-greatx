import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

var stars = Row(
  mainAxisSize: MainAxisSize.min,
  children: [
    Icon(Icons.star, color: Colors.green[500]),
    Icon(Icons.star, color: Colors.green[500]),
    Icon(Icons.star, color: Colors.green[500]),
    Icon(Icons.star, color: Colors.black),
    Icon(Icons.star, color: Colors.black),
  ],
);

final ratings = Container(
  // padding: EdgeInsets.all(10),
  child: Row(
    mainAxisSize: MainAxisSize.min,
    children: [
      stars,
      Text(
        '170 Reviews',
        style: TextStyle(
          color: Colors.black,
          fontWeight: FontWeight.w800,
          fontFamily: 'Roboto',
          letterSpacing: 0.5,
          fontSize: 12,
        ),
      ),

    ],
  ),
);


class MyHomePage extends StatelessWidget {
  final String title;
  MyHomePage({Key key, this.title}):super(key: key);
  // ignore: non_constant_identifier_names
  var Items =
      Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            'Heading Strawberry',
            style: TextStyle(
                color: Colors.black,
                fontSize: 20,
                fontWeight: FontWeight.bold,
            ),
          ),
          Text(
                'Note: Row and Column.',
            style: TextStyle(
              fontSize: 12,
              color: Colors.black,
            ),
          ),
          ratings,
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Expanded(
                  child: Column(
                    children: [
                      Icon(Icons.wifi),
                      Text('PREP:', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16)),
                      Text('20 mn'),
                    ],
                  ),
              ),
              Expanded(
                child: Column(
                  children: [
                    Icon(Icons.wifi),
                    Text('PREP:', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16)),
                    Text('1 hr'),
                  ],
                ),
              ),
              Expanded(
                child: Column(
                  children: [
                    Icon(Icons.wifi),
                    Text('PREP:', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16)),
                    Text('1 hr'),
                  ],
                ),
              ),
            ],
          ),
        ],
      );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Layout')),
      body: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Expanded(child: Image.asset('assets/s.jpeg')),
            Expanded(child: Items),
          ],
        ),
    );
  }
}


import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;
  // @override
  // _MyHomePageState createState() => _MyHomePageState();

  @override
  ActCount createState() => ActCount();
}

class ActCount extends State<MyHomePage>{
  int _counter = 0;
  void _increasingCount() {
    setState(() {
      return _counter++;
    });
  }

  void _descreasingCount()
  {
    setState(() {
      return _counter--;
    });
  }

  @override
  Widget build(BuildContext context)
  {
    return Scaffold(
      appBar: AppBar(
        title: Text("Counter me"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('Number of click: '),
            Text('$_counter'),
          ],
        ),
      ),
      // floatingActionButton: FloatingActionButton(
      //   onPressed: _increasingCount,
      //   child: Icon(Icons.add),
      // ),

      floatingActionButton: FloatingActionButton(
        onPressed: _descreasingCount,
        child: Icon(Icons.remove),
      ),
    );
  }
}



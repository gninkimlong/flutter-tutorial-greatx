import 'package:flutter/material.dart';


void main()
{
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: CardList(title: 'CardListTitle'),
    );
  }
}

class CardList extends StatelessWidget {
  final String title;
  CardList({Key key, this.title}):super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(this.title)),
      body: _cardItem(),
    );
  }
}

Widget _cardItem() => SizedBox(
  height: 220,
  child: Card(
    child: Column(
      children: [
        ListTile(
          title: Text("Title A"),
          subtitle: Text("Description of subtitle A"),
          leading: Icon(
            Icons.article,
          ),
        ),
        ListTile(
          title: Text("Title B"),
          subtitle: Text("Description of subtitle B"),
          leading: Icon(
            Icons.article,
          ),
        )
      ],
    ),
  ),
);



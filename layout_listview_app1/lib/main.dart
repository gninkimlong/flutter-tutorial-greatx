
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

Widget _displayResult(BuildContext context, int index)
{

  final stackbar = SnackBar(content: Text('Tap: $index'));
  ScaffoldMessenger.of(context).showSnackBar(stackbar);
  print('You click item: $context');
}

final List<String> entries = <String>['A', 'B', 'C'];
final List<int> colorCodes = <int>[600, 500, 100];
Widget listViewItem() => ListView.separated(
    itemBuilder: (BuildContext context, int index){
      // return Container(
      //   height: 50,
      //   color: Colors.amber[colorCodes[index]],
      //   child: Center(child: Text('Entry ${entries[index]}')),
      // );
      return ListTile(
        title: Text('item ${entries[index]}'),
        onTap: ()=>_displayResult(context, index),

      );
    },
    separatorBuilder: (BuildContext context, int index) => const Divider(),
    itemCount: entries.length,

);


class MyHomePage extends StatelessWidget {
  final String title;
  MyHomePage({Key key, this.title}):super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('ListView Builder')),
      body: listViewItem(),
    );
  }
}

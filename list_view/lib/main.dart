import 'package:flutter/material.dart';

void main() {
  runApp(listViewApp());
}
class listViewApp extends StatelessWidget
{
  @override
  Widget build(BuildContext context) {
    Color color = Theme.of(context).primaryColor;
    Widget buttonList = Container(
      margin: const EdgeInsets.all(5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _decorateColoumnListView(color, Icons.call, 'CALL'),
          _decorateColoumnListView(color, Icons.near_me, 'ROUTE'),
          _decorateColoumnListView(color, Icons.share, 'SHARE'),
        ],
      ),

    );

    return MaterialApp(
      title: 'List view App',
      home: Scaffold(
        appBar: AppBar(
          title: Text('List View App Devmo'),
        ),
        body: ListView(
          padding: EdgeInsets.all(19),
          children: [
            buttonList,
            actionButton(),
          ],
        ),
      ),
    );
  }
}

Column _decorateColoumnListView(Color color, IconData icon, String label)
{
  return Column(
    mainAxisSize: MainAxisSize.min,
    mainAxisAlignment: MainAxisAlignment.center,
    children: [

      Icon(icon, color: color),
      Container(
        padding: const EdgeInsets.all(10),
        margin: const EdgeInsets.only(top: 8),
        child: Text(
          label,
          style: TextStyle(
            fontSize: 12,
            fontWeight: FontWeight.bold,
            color: color,
          ),
        ),
      ),
    ],
  );
}


class actionButton extends StatefulWidget
{
  _actionButtonState createState() =>_actionButtonState();
}
class _actionButtonState extends State<actionButton>
{
  bool _isFavorited = true;
  int _count = 0;
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          padding: EdgeInsets.all(10),
          child: IconButton(
            padding: EdgeInsets.all(0),
            alignment: Alignment.centerRight,
            icon: (_isFavorited ? Icon(Icons.star) : Icon(Icons.star_border)),
            color: Colors.lightBlue,
            onPressed: _phoneCall,
          ),
        ),
        SizedBox(
          width: 18,
          child: Container(
            child: Text('$_count'),
          ),
        ),
      ],
    );
  }
  void _phoneCall()
  {
    setState(() {
      _count += 1;
    });
  }
}
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: ProHome(title: 'Flutter Demo Home Page'),
    );
  }
}

class ProHome extends StatelessWidget {

  final String title;
  @override
  ProHome({Key key, this.title}): super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(this.title)),
      body: Container(
        // child: Text(
        //   "This is second page",
        // ),
        padding: EdgeInsets.all(10),
        child: Text.rich(
          TextSpan(

            children: <TextSpan>[
              TextSpan(text: "Hello world", style: TextStyle(fontSize: 15)),
              TextSpan(text: "Welcome home", style: TextStyle(fontSize: 13, fontWeight: FontWeight.normal)),
            ],


          ),
          overflow: TextOverflow.ellipsis,
          textAlign: TextAlign.center,
        ),

      ),
    );
  }
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mvvm_news/constant/constant.dart';
import 'package:mvvm_news/viewmodel/listviewmodel.dart';
import 'package:mvvm_news/widget/newsgrid.dart';
import 'package:provider/provider.dart';

class NewsScreen extends StatefulWidget{
  _NewsScreenState createState() =>_NewsScreenState();
}

class _NewsScreenState extends State<NewsScreen>
{

  //This state will occure when page load
  //First open app
  @override
  void initState()
  {
    super.initState();
    Provider.of<ListViewModel>(context, listen: false).fetchTopNewsHeadlines();
  }

  @override
  Widget build(BuildContext context) {

    var listviewmodel = Provider.of<ListViewModel>(context);
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        title: Text('GreatX Institute'),
        actions: <Widget>[
          PopupMenuButton(
            onSelected: (country){
              listviewmodel.fetchNewsByCountry(Constants.countries[country]);
            },
            icon: Icon(Icons.more_vert),
            itemBuilder: (_){
              return Constants.countries.keys.map((v) => PopupMenuItem(value: v, child: Text(v), )).toList();
            }
          ),
        ],
      ),

      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
                padding: const EdgeInsets.only(left: 30, bottom: 15, top: 15),
                child: Text('Headlines', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
            ),

            Expanded(
                child: NewsGrid(articles: listviewmodel.articles)
            )
          ],
        ),
      ),
    );
  }



}
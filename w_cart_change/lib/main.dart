import 'package:flutter/material.dart';
class Product {
  const Product({this.name, this.txt});
  final String name;
  final String txt;
}
typedef void CartChangedCallback(Product product, bool inCart, String str);

class ShoppingListItem extends StatelessWidget {

  const ShoppingListItem({Key key, this.product, this.inCart, this.onCartChanged}):super(key: key);
  final Product product;
  final bool inCart;
  final CartChangedCallback onCartChanged;

  Color _getColor(BuildContext context)
  {
    return inCart?Colors.black54: Theme.of(context).primaryColor;
  }

  TextStyle _getTextStyle(BuildContext context)
  {
    if(!inCart) return null;
    return TextStyle(
      color: Colors.black54,
      decoration: TextDecoration.lineThrough,
    );
  }

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: (){
        onCartChanged(product, inCart, 'KIMLONG');
      },
      leading: CircleAvatar(
        backgroundColor: _getColor(context),
        child: Text(product.name[0]),
      ),
      title: Text('${product.name} - ${product.txt}', style: _getTextStyle(context)),
    );
  }
}

class ShoppingList extends StatefulWidget {
  ShoppingList({Key key, this.products}):super(key: key);
  final List<Product> products;
  @override
  _ShoppingListState createState() => _ShoppingListState();
}

class _ShoppingListState extends State<ShoppingList> {
  Set<Product> _shoppingCart = Set<Product>();
  void _handleCartChange(Product product, bool inCart, String txt)
  {
    setState(() {
      if(!inCart)
      {
          _shoppingCart.add(product);
      }else{
          _shoppingCart.remove(product);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Shopping List'),
      ),
      body: ListView(
        padding: EdgeInsets.symmetric(vertical: 8.0),
        children: widget.products.map((Product product){
          return ShoppingListItem(
            product: product,
            inCart: _shoppingCart.contains(product),
            onCartChanged: _handleCartChange,

          );
        }).toList(),
      ),

    );
  }
}

void main() {
  runApp(MaterialApp(
    title: 'Shopping App',
    home: ShoppingList(
      products: <Product>[
        Product(name: 'Eggs', txt: 'Big'),
        Product(name: 'Flour', txt: 'Chilly'),
        Product(name: 'Chocolate chips', txt: 'Tomato'),
        Product(name: 'BBQ', txt: 'Beaf'),
        Product(name: 'Chicken Wings', txt: 'Sweet'),
        Product(name: 'African Wing', txt: 'Footbal'),
      ],
    ),
  ));
}






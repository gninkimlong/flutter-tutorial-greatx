import 'package:flutter/material.dart';
void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:AppBar(
          title: Text(
              'Materail App',

          )
      ),
      // body: Center(
      //   child: Text('Hello world',
      //     textDirection: TextDirection.ltr,
      //     style: TextStyle(
      //         fontSize: 32,
      //         color: Colors.black87
      //     ),
      //   ),
      // ),

      body: ImageData(),
    );
  }
}

class ImageData extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Expanded(child: Image.asset('assets/1.jpeg', width: 80, height: 80)),
        Expanded(flex: 2, child: Image.asset('assets/2.jpeg', width: 80, height: 80)),
        Expanded(child: Image.asset('assets/3.jpeg', width: 80, height: 80)),
      ],
    );
  }
}



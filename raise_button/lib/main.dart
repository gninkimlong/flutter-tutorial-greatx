import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: RaiseButton(),
    );
  }
}

class RaiseButton extends StatefulWidget {
  @override
  _RaiseButtonState createState() => _RaiseButtonState();
}

class _RaiseButtonState extends State<RaiseButton> {
  int count=0;
  void increasingCount()
  {
    setState(() {
      count++;
    });
  }
  void displayCount()
  {
    setState(() {
      print('you click me-${count++}');
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Raise Button'),
      ),
      body: new Center(
        child: new RaisedButton(onPressed:()=> increasingCount(),
          child: new Text('You click me-${count}'),
        ),

      ),
    );
  }
}



